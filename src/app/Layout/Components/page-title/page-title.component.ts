import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-page-title',
  templateUrl: './page-title.component.html',
})
export class PageTitleComponent {

  @Input() heading = 'Dashboard';
  @Input() subheading ='base content for website design';
  @Input() icon;
  

}
