import {Component, HostBinding, OnInit} from '@angular/core';
import {ThemeOptions} from "../../../../../theme-options";
import {select} from "@angular-redux/store";
import {Observable} from "rxjs";

@Component({
  selector: 'app-mega-menu',
  templateUrl: './mega-menu.component.html',
})
export class MegamenuComponent implements OnInit {

  constructor(public globals: ThemeOptions) { }

  ngOnInit() {
  }

  toggleDrawer() {
    this.globals.toggleDrawer = !this.globals.toggleDrawer;
  }
  @HostBinding('class.isActive')
  get isActiveAsGetter() {
    return this.isActive;
  }

  isActive: boolean;

  @select('config') public config$: Observable<any>;

  toggleSidebarMobile() {
    this.globals.toggleSidebarMobile = !this.globals.toggleSidebarMobile;
  }

  toggleHeaderMobile() {
    this.globals.toggleHeaderMobile = !this.globals.toggleHeaderMobile;
  }
}
