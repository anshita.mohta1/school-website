import {Component} from '@angular/core';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
})
export class CarouselComponent {

  heading = 'News';
  subheading = 'Latest Activity log';
  icon = 'pe-7s-album icon-gradient bg-sunny-morning';

  // images = [1, 2, 3].map(() => `https://picsum.photos/900/500?random&t=${Math.random()}`);

  slides = [
    {img: './assets/images/frontnews/01.jpg'},
    {img: './assets/images/frontnews/02.jpg'},
    {img: './assets/images/frontnews/03.jpg'},
    {img: './assets/images/frontnews/04.jpg'},
    {img: './assets/images/frontnews/06.jpg'},
    {img: './assets/images/frontnews/07.jpg'},
    {img: './assets/images/frontnews/08.jpg'},

  ];
  slidesfrist = [
    {img: './assets/images/newsforth/01.jpg'},
    {img: './assets/images/newsforth/02.jpg'},
    {img: './assets/images/newsforth/03.jpg'},
    {img: './assets/images/newsforth/04.jpg'},
    {img: './assets/images/newsforth/05.jpg'},
  ];
  slidesforth = [
    {img: './assets/images/homepageslider/001.jpg', name: 'SCHOOL CAMP'},
    {img: './assets/images/homepageslider/003.jpg', name: 'FOUNDER AND DIRECTOR'},
    {img: './assets/images/homepageslider/004.jpg', name: 'ADVISORY BOARD'},
    {img: './assets/images/homepageslider/005.jpg', name: 'EVENTS'},
    {img: './assets/images/homepageslider/006.jpg', name: 'EVENTS'},
    {img: './assets/images/homepageslider/007.jpg', name: 'OUR CAMPUSES'},
    {img: './assets/images/homepageslider/008.jpg', name: 'FACILITIES'},
    {img: './assets/images/homepageslider/009.jpg', name: 'FACILITIES'},
    {img: './assets/images/homepageslider/10.jpg', name: "CULTURAL FEST"},
    {img: './assets/images/homepageslider/11.jpg', name: 'RESULTS'},
    {img: './assets/images/homepageslider/12.jpg', name: 'CAMPUS FACILITIES'},
    {img: './assets/images/homepageslider/13.jpg', name: 'CELEBRATIONS'},
    {img: './assets/images/homepageslider/14.jpg', name: 'CELEBRATIONS'},
  ]
  slideConfig = {
    slidesToShow: 1,
    dots: true,
    autoplay: true,
  };

  slideConfig1 = {
    slidesToShow: 1,
    dots: false,
    autoplay: true,
  }

  slideConfig2 = {
    className: 'center',
    centerMode: true,
    infinite: true,
    centerPadding: '60px',
    slidesToShow: 3,
    speed: 500,
    dots: true,
    autoplay: true,
  };

  slideConfig3 = {
    autoplay: true,
    dots: true,
    infinite: false,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 4,
    initialSlide: 0,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          initialSlide: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  };

  slideConfig4 = {
    slidesToShow: 3,
    dots: true,
  };

  slideConfig5 = {
    className: 'slider variable-width',
    dots: true,
    infinite: true,
    centerMode: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    variableWidth: true
  };

  slideConfig6 = {
    className: 'center',
    infinite: true,
    slidesToShow: 1,
    speed: 500,
    adaptiveHeight: true,
    dots: true,
  };

}
