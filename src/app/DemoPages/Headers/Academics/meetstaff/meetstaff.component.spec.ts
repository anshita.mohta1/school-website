import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MeetstaffComponent } from './meetstaff.component';

describe('MeetstaffComponent', () => {
  let component: MeetstaffComponent;
  let fixture: ComponentFixture<MeetstaffComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MeetstaffComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MeetstaffComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
