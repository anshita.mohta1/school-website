import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BoardofmanagementComponent } from './boardofmanagement.component';

describe('BoardofmanagementComponent', () => {
  let component: BoardofmanagementComponent;
  let fixture: ComponentFixture<BoardofmanagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BoardofmanagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BoardofmanagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
