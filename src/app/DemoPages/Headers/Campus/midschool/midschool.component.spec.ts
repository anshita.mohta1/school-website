import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MidschoolComponent } from './midschool.component';

describe('MidschoolComponent', () => {
  let component: MidschoolComponent;
  let fixture: ComponentFixture<MidschoolComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MidschoolComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MidschoolComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
