import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrimaryschoolComponent } from './primaryschool.component';

describe('PrimaryschoolComponent', () => {
  let component: PrimaryschoolComponent;
  let fixture: ComponentFixture<PrimaryschoolComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrimaryschoolComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrimaryschoolComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
