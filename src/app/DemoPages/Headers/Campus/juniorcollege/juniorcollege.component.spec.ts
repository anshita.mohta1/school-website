import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JuniorcollegeComponent } from './juniorcollege.component';

describe('JuniorcollegeComponent', () => {
  let component: JuniorcollegeComponent;
  let fixture: ComponentFixture<JuniorcollegeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JuniorcollegeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JuniorcollegeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
