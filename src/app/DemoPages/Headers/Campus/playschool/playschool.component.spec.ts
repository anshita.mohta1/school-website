import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayschoolComponent } from './playschool.component';

describe('PlayschoolComponent', () => {
  let component: PlayschoolComponent;
  let fixture: ComponentFixture<PlayschoolComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlayschoolComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayschoolComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
