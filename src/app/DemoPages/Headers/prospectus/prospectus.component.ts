import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-prospectus',
  templateUrl: './prospectus.component.html',
  styleUrls: ['./prospectus.component.sass']
})
export class ProspectusComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  slidersthird = [
    {img: './assets/images/newssecond/01.jpg'},
    {img: './assets/images/newssecond/02.jpg'},
    {img: './assets/images/newssecond/03.jpg'},
    {img: './assets/images/newssecond/04.jpg'},
    {img: './assets/images/newssecond/05.jpg'},
    {img: './assets/images/newssecond/06.jpg'},
    {img: './assets/images/newssecond/07.jpg'},
    {img: './assets/images/newssecond/08.jpg'},
    {img: './assets/images/newssecond/09.jpg'},
    {img: './assets/images/newssecond/10.jpg'},
    {img: './assets/images/newssecond/11.jpg'},
    {img: './assets/images/newssecond/12.jpg'},
    {img: './assets/images/newssecond/13.jpg'},
    {img: './assets/images/newssecond/14.jpg'},
    {img: './assets/images/newssecond/15.jpg'},
    {img: './assets/images/newssecond/16.jpg'},
    {img: './assets/images/newssecond/17.jpg'},
  ]

  slideConfig = {
    slidesToShow: 1,
    dots: true,
    autoplay: true,
  };

}
