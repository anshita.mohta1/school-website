import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.sass']
})
export class EventsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  slidersthird = [
    {img: './assets/images/extrac/dance.JPG', name: "ANNUAL FUNCTION"},
    {img: './assets/images/extrac/envday.jpg',  name: "ENVIRONMENT DAY"},
    {img: './assets/images/extrac/gurupurnima.jpg', name: "GURU PURNIMA CELEBRATION"},
    {img: './assets/images/extrac/aero.jpg', name: "AEROBIC CLASSES"},
    {img: './assets/images/extrac/investitureceremony.jpg', name: "INVESTITURE CEREMONY"},
    {img: './assets/images/extrac/janmashtmi.jpg', name: "JANMASHTMI CELEBRATION"},
    {img: './assets/images/extrac/rangoli2.jpg', name: "RANGOLI COMPETITION"},
    {img: './assets/images/extrac/schoolfar.jpg', name: "CHILDREN FAIR"},
    {img: './assets/images/extrac/sport.jpg', name: "SPORTS DAY"},
    {img: './assets/images/extrac/teachers.jpg', name: "TEACHER'S DAY CELEBRATION"},
    {img: './assets/images/extrac/workshop2.jpg', name: "WORKSHOP"},
  ]

  slideConfig = {
    slidesToShow: 1,
    dots: true,
    autoplay: true,
  };

}
