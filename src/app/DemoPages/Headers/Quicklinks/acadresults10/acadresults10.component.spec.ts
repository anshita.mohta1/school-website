import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Acadresults10Component } from './acadresults10.component';

describe('Acadresults10Component', () => {
  let component: Acadresults10Component;
  let fixture: ComponentFixture<Acadresults10Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Acadresults10Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Acadresults10Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
