import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AcadscholarshipsComponent } from './acadscholarships.component';

describe('AcadscholarshipsComponent', () => {
  let component: AcadscholarshipsComponent;
  let fixture: ComponentFixture<AcadscholarshipsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AcadscholarshipsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AcadscholarshipsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
