import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Acadresults12Component } from './acadresults12.component';

describe('Acadresults12Component', () => {
  let component: Acadresults12Component;
  let fixture: ComponentFixture<Acadresults12Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Acadresults12Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Acadresults12Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
