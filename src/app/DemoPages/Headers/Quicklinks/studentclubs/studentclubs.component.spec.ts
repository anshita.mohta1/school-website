import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentclubsComponent } from './studentclubs.component';

describe('StudentclubsComponent', () => {
  let component: StudentclubsComponent;
  let fixture: ComponentFixture<StudentclubsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentclubsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentclubsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
