import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonalitydevelopmentComponent } from './personalitydevelopment.component';

describe('PersonalitydevelopmentComponent', () => {
  let component: PersonalitydevelopmentComponent;
  let fixture: ComponentFixture<PersonalitydevelopmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonalitydevelopmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonalitydevelopmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
