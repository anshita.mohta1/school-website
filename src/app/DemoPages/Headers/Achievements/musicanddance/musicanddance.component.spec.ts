import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MusicanddanceComponent } from './musicanddance.component';

describe('AcademicComponent', () => {
  let component: MusicanddanceComponent;
  let fixture: ComponentFixture<MusicanddanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MusicanddanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MusicanddanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
