import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-latestnews',
  templateUrl: './latestnews.component.html',
  styleUrls: ['./latestnews.component.sass']
})
export class LatestnewsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  slideConfig2 = {
    slidesToShow: 2,
    dots: false,
    autoplay: true,
  }

  sliderslatestnews = [
    {img: './assets/images/frontnews/01.jpg'},
    {img: './assets/images/frontnews/02.jpg'},
    {img: './assets/images/frontnews/03.jpg'},
    {img: './assets/images/frontnews/04.jpg'},
    {img: './assets/images/frontnews/06.jpg'},
    {img: './assets/images/frontnews/07.jpg'},
    {img: './assets/images/frontnews/08.jpg'},

  ];
}
